import feedparser
import sqlite3 as sl
import requests

WEBHOOK_URL = "https://hooks.slack.com/services/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"

con = sl.connect('aws.db')
with con:
  con.execute("""
    CREATE TABLE IF NOT EXISTS ALERTS (
      id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      guid TEXT
    );
  """)

aws_feed = feedparser.parse("https://status.aws.amazon.com/rss/all.rss")
entry = aws_feed.entries

for entry in aws_feed.entries:
  with con:
    data = con.execute("SELECT id FROM ALERTS WHERE guid = ?", (entry['id'],))

    has_processed = False
    for dt in data:
      has_processed = True

    if has_processed:
      continue

  requests.post(WEBHOOK_URL, json={
    "text": entry['title'],
    "attachments": [{
      "color": "#710c04",
      "blocks": [
        {
          "type": "context",
          "elements": [
            {
              "type": "mrkdwn",
              "text": "Hey <@UMFFJA0DA>"
            }
          ]
        },
        {
          "type": "context",
          "elements": [
            {
              "type": "image",
              "image_url": "https://api.slack.com/img/blocks/bkb_template_images/notificationsWarningIcon.png",
              "alt_text": "notifications warning icon"
            },
            {
              "type": "mrkdwn",
              "text": "*%s*" % entry['title']
            }
          ]
        },
        {
          "type": "divider"
        },
        {
          "type": "section",
          "text": {
            "type": "mrkdwn",
            "text": ":calendar: |   %s " % entry['published']
          }
        },
        {
          "type": "section",
          "text": {
            "type": "mrkdwn",
            "text": "%s" % entry['summary']
          },
          "accessory": {
            "type": "image",
            "image_url": "https://pbs.twimg.com/profile_images/1473756532827246593/KRgw2UkV_400x400.jpg",
            "alt_text": "aws"
          }
        },
        {
          "type": "actions",
          "block_id": "actionblock789",
          "elements": [
            {
              "type": "button",
              "text": {
                "type": "plain_text",
                "text": "See"
              },
              "url": "http://status.aws.amazon.com/",
              "style": "danger"
            }
          ]
        }
      ]
    }]
  })

  sql = 'INSERT INTO ALERTS (guid) values(?)'
  data = [
      (entry['id'],),
  ]
  with con:
    con.executemany(sql, data)